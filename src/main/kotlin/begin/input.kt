package begin

import operations.*

class begin {
    fun input () {
        val numberone : Int
        val numbertwo : Int
        var buf : String
        var valReturnsInput: Array<Int> = arrayOf(0)


        print("введите первое число ")
        val name = readLine()
        buf = name.toString()
        numberone = buf.toInt()

        print("введите второе число ")
        val name1 = readLine()
        buf = name1.toString()
        numbertwo = buf.toInt()

        valReturnsInput = arrayOf(numberone, numbertwo)
        optionSelection(valReturnsInput)
    }

    fun optionSelection (intArray: Array<Int>) {
        print(  "1 - Сложение\n" +
                "2 - Вычитание\n" +
                "3 - Умножение\n" +
                "4 - Деление\n" +
                "5 - Возведение в степень\n" +
                "6 - Извлечение корня\n" +
                "Выберете операцию: ")
        val valReturnOptionSelect = readLine()
        if (valReturnOptionSelect != null) {
            check(valReturnOptionSelect, intArray)
        }


    }
    fun check (string : String, array : Array<Int>) {
        val op = operations()
        when (string) {
            "1" -> op.summation (array)        //сложение
            "2" -> op.subtraction (array)      //вычитание
            "3" -> op.multiplaction (array)    //умножение
            "4" -> op.division (array)         //деление
            "5" -> op.degree (array)           //возведение в степень
            "6" -> op.extraction (array)       //извлечение корня
        }
    }
}